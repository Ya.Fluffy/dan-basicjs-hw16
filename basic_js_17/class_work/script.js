// **
//  * Завдання 1.
//  *
//  * Написати програму для нагадувань.
//  *
//  * Усі модальні вікна реалізувати через alert.
//  *
//  * Умови:
//  * - Якщо користувач не ввів повідомлення для нагадування - вивести alert з повідомленням "Ведіть текст нагадування";
//  * - Якщо користувач не ввів значення секунд, через скільки потрібно вивести нагадування -
//  * вивести alert з повідомленням «Час затримки має бути більше однієї секунди.»;
//  * - Якщо всі дані введені правильно, при натисканні на кнопку «нагадати» необхідно її блокувати так,
//  * щоб повторний клік став можливим після повного завершення поточного нагадування;
//  * - після цього повернути початкові значення обох полів;
//  * - Створювати нагадування, якщо всередині одного з двох елементів input натиснути клавішу Enter;
//  * - Після завантаження сторінки встановити фокус у текстовий input.
//  */

const inputReminder = document.querySelector("#reminder");
console.log(inputReminder);
const inputTime = document.querySelector("#seconds");
let textReminder = "";
// textReminder.addEventListener('input', (e)=>{
// textReminder = e.target.value
// console.log(textReminder)
// })

const btn = document.querySelector("button");

btn.addEventListener("click", reminder);

function reminder() {
  if (!inputReminder) {
    alert("Ведіть текст нагадування");
  }
  textReminder = inputReminder.value;
  if (inputTime.value < 1) {
    alert("ас затримки має бути більше однієї секунди.");
  }
  console.log(inputReminder.value);
  btn.setAttribute("disabled", true);
  setTimeout(() => {
    alert(textReminder);
    btn.removeAttribute("disabled");
    textReminder = "";
    console.log("textReminder", textReminder);
  }, +inputTime.value * 1000);

  inputReminder.value = "";
  inputTime.value = 0;
}

window.addEventListener("keyup", (e) => {
  if (e.key === "Enter") {
    reminder();
  }
});

window.addEventListener("DOMContentLoaded", () => {
  console.log(inputReminder);
  inputReminder.focus();
});

/**
 * Завдання 2.
 *
 * Зімітувати завантаження даних.
 *
 * При відкритті сторінки показувати спіннер протягом 5 секунд.
 *
 * Потім відобразити мок.
 */

const section = document.querySelector("section");
section.classList.add("hidden");
const loading = document.querySelector(".loading");

window.addEventListener("load", () => {
  setTimeout(() => {
    section.classList.remove("hidden");
    loading.classList.add("hidden");
  }, 5000);
});
