/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
- setInterval - повторює запуск виконання функціїї через заданий інтервал
- setTimeout - запускає виконання функціїї через заданий іентервал лише один раз.

2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
- не можна, так як ці функції будуть виконані негайно після прочитання всього скрипта, тобто це залежить від заповнення стеку виклику

3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
setTimeout та setInterval присвоїти змінній, на яку потім примінити метод clear()
*/ 


// Практичне завдання 1: 

// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.
const div = document.createElement('div');
div.textContent = 'Hello World!';
const button = document.createElement('button');
button.innerText = 'Change text';
document.body.prepend(button)
document.body.prepend(div)

button.addEventListener('click', () => {
setTimeout(function change(){
        div.textContent = 'The text has been changed'
}, 3000)

setTimeout(()=>{
    div.textContent = 'Hello World!'
    }, 5000)
    
})



// Практичне завдання 2: 

// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".

const div1 = document.createElement('div');
let countSec = 10;
div1.innerHTML = countSec;
div1.style.marginTop = '30px';
document.body.append(div1)

const setIntervalID = setInterval(function count(){
    countSec--;
    if(countSec > 0){
        div1.innerHTML = countSec;
    } else {
        div1.innerHTML = 'Зворотній відлік завершено';
        clearInterval(setIntervalID)
    }
}, 1000)



