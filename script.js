/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
= запобігає виконання певних дій барузером, наприклад відсилання форми, або відкриття посилання, або якщо треба запобігти деяким діям користувача з клавуатурою.

2. В чому сенс прийому делегування подій?
= це спосіб зробити код трохи легшим, вішаючи один обробник подій на блок з колекцією елемементів.

3. Які ви знаєте основні події документу та вікна браузера? 
= DOMContentLoaded, load, deforeunload or unload 



Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/
const tabs = document.querySelectorAll(".tabs-title");
console.log(tabs);
const texts = document.querySelectorAll(".tabs-content li");
console.log(texts);
const tabsParent = document.querySelector('.tabs')

for (let i = 0; i < tabs.length; i++) {
  // console.log('i', tabs[i].textContent)
  for (let j = 0; j < texts.length; j++) {
    // console.log('j', texts[j])
    texts[i].setAttribute("data-name", tabs[i].textContent);
    if (tabs[i].classList.contains("active")) {
      texts[i].classList.remove("none");
    } else {
      texts[i].classList.add("none");
    }
  }
}

tabsParent.addEventListener("click", openTab);
// tabs.forEach((tab) => {
//   tab.addEventListener("click", openTab);
// });

function openTab(e) {
//   console.log(e.target.textContent);
  tabs.forEach((tab) => {
    // console.log(tab);
    if (tab === e.target) {
      tab.classList.add("active");
    } else {
      tab.classList.remove("active");
    }
  });
  texts.forEach((text) => {
    const dataName = text.getAttribute("data-name");
    // console.log(dataName)
    if (e.target.textContent === dataName) {
      text.classList.remove("none");
    } else {
      text.classList.add("none");
    }
  });
}
